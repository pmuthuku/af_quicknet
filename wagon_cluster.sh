#!/bin/bash

# Assumes that the code will be run in the voice directory

CODE_PATH=/home/pmuthuku/code/AF_quicknet/

CG_TMP=jnk_$$

cd MCEP_AF_perphone/


# Concatenate all the MCEPs into one file
cat *.mcep | perl -lane 'print "@F[1..$#F]";' | $ESTDIR/bin/ch_track -s 0.005 -itype ascii -otype est_binary -o mcep_all
# Concatenate all AFs into one file and add the vector index for the first column
cat *.af | perl -e '
$count=0;
while(<STDIN>){
   print "$count\t$_";
   $count++;
}' > af_all

cp -p $CODE_PATH/af_mcep_cluster.desc .


$ESTDIR/bin/wagon -track_feats 1-50 -vertex_output mean -desc af_mcep_cluster.desc -data af_all -track mcep_all -output tree.tree -stop 500

exit

#################################################### Disjoint here ##########################

#Approximately maximum number of leaves of the tree
num_induced_phones=`wc -l 35_ph.tree | awk '{print $1}'`

cp 35_ph.tree $CG_TMP.tree
for (( i=1; i < $num_induced_phones; i++))
do
    sed '0,/(((([0-9]/s/(((([0-9]/((((ip'${i}') (9/' $CG_TMP.tree > $CG_TMP.treee
    cp $CG_TMP.treee $CG_TMP.tree
done
cp $CG_TMP.tree 35_ph_subbed.tree

rm -f $CG_TMP.*
#rm -f mcep_all af_all

