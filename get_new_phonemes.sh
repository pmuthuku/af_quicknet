#!/bin/bash

PROMPTFILE="$2"
NUMSTOPS="$1"

CODE_PATH=/home/pmuthuku/code/AF_quicknet/


if [ "$PROMPTFILE" = "" ]
then
    PROMPTFILE=etc/txt.done.data
fi


CG_TMP=jnk_$$


if [ -f etc/new_txt.done.data ]
then
    echo "Deleting previous new_txt.done.data"
    rm etc/new_txt.done.data
fi

cat $PROMPTFILE |
awk '{print $2}' |
while read i
do

    fname=$i
    echo "Predicting phone labels for $fname"

    cat MCEP_AF_perphone/$fname.af | perl -e '
       $count=0;
       while(<STDIN>){
          print "$count\t$_";
          $count++;
       }'  > $CG_TMP.af_feats

    $ESTDIR/bin/wagon_test -desc MCEP_AF_perphone/af_mcep_cluster.desc -data $CG_TMP.af_feats -tree phone_trees/STOP$NUMSTOPS-subbed.tree -predict |  awk '{print $1}' | sed 's/[()]//g' | perl -pe 's/\n/ /' > $CG_TMP.phnemes

    echo "( $fname \"`cat $CG_TMP.phnemes`\" ) " >> etc/new_txt.done.data

done

rm -f $CG_TMP.*
