#!/bin/bash
# This script will config the job for the LTI cluster
# Usage: ./configJob array_job_file h_rt h_vmem smp

if [ $# -lt 2 ]
then
	echo "Usage: $0 array_job_file script_file [nLinesPerJob=1] [h_rt] [h_vmem] [smp]"
	exit -1
fi

#$ -N txyc.1.sh
#$ -S /bin/bash
#$ -e /home/baolei/MED11/mosift/sh/txyc/txyc.1.sh.err
#$ -o /home/baolei/MED11/mosift/sh/txyc/txyc.1.sh.log
#$ -l pmem=512mb
#$ -l walltime=04:00:00
#$ -l nodes=1:ppn=1


# Parse the input
array_job_file=$1
if [ -f $array_job_file ]
then
	rm $array_job_file
fi
array_job_dir=$(dirname $array_job_file)
if [ "$array_job_dir" == "." ]
then
	array_job_file=$PWD/$array_job_file
fi
script_file=$2
if [ ! -e $script_file ]
then
	echo "Script file $script_file does not exist!!"
	exit -1
fi
script_dir=$(dirname $script_file)
if [ "$script_dir" == "." ]
then
	script_file=$PWD/$script_file
fi
if [ $# -ge 3 ]
then
	nLinesPerJob=$3
else
	nLinesPerJob=1
fi
if [ $# -ge 4 ]
then
	walltime=$4
else
	walltime=01:00:00 # Default run time
fi
if [ $# -ge 5 ]
then
	pmem=$5
else
	pmem=1g # Default required memory
fi
if [ $# -ge 6 ]
then
	smp=$6
else
	smp=1
fi

array_job_name=$(basename $array_job_file)
# Set the job name
echo "#PBS -N $array_job_name" >> $array_job_file

# Use the bourne shell
echo "#PBS -S /bin/bash" >> $array_job_file

# Set the destination for the job's output
job_dir=$(dirname $array_job_file)
if [ ! -n $job_dir ]
then
	echo "#PBS -e $HOME/$array_job_name.err" >> $array_job_file
	echo "#PBS -o $HOME/$array_job_name.log" >> $array_job_file
else
	echo "#PBS -e $array_job_file.err" >> $array_job_file
	echo "#PBS -o $array_job_file.log" >> $array_job_file
fi

# Tell SGE how much memory you expect to use. Use units of 'b','k', 'm' or 'g'.
echo "#PBS -l pmem=$pmem" >> $array_job_file

# Tell SGE the anticipated run-time for your job, where required time is formatted HH:MM:SS
# This is the time for one single job, not the whole array job
echo "#PBS -l walltime=$walltime" >> $array_job_file

# Specify the number of cpus
echo "#PBS -l nodes=1:ppn=$smp" >> $array_job_file

# Specify the array job number
nLines=$(cat $script_file | wc -l)
nJobs=$(((nLines+nLinesPerJob-1)/$nLinesPerJob))
echo "#PBS -t 1-$nJobs" >> $array_job_file

# get particular line of script given the SGE task ID
echo "start_idx=\$(((\$PBS_ARRAYID-1)*$nLinesPerJob+1))" >> $array_job_file
echo "for ((i=\$start_idx;i<\$start_idx+$nLinesPerJob;i++))" >> $array_job_file
echo "do" >> $array_job_file
echo "if [ \$i -le $nLines ]" >> $array_job_file
echo "then" >> $array_job_file
echo "script=\$(sed -n -e \"\$i p\" $script_file)" >> $array_job_file
echo "eval \$script" >> $array_job_file
echo "fi" >> $array_job_file
echo "done" >> $array_job_file

# Change the mode of the script
chmod +x $array_job_file

