#!/bin/python

import sys as sys



if len(sys.argv) >= 2:
    sent_no = sys.argv[1]
    preset_sent_no = 1
else:
    sent_no = 0
    preset_sent_no = 0

 
frame_no=0

for line in sys.stdin:

    sys.stdout.write(str(sent_no))
    sys.stdout.write(" ")
    sys.stdout.write(str(frame_no))
    sys.stdout.write(" ")
    sys.stdout.write(line)

    frame_no = frame_no + 1
    
    if preset_sent_no < 0.5:
        if frame_no >= 501:
            sent_no = sent_no + 1
            frame_no=0

