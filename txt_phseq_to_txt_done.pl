#!/bin/perl

use strict;
use warnings;

my @phnes;
my $k;

while(<STDIN>){
   
    chomp;
    $_ =~ s/pau //g;
    $_ =~ s/ssil //g;
  
    
    @phnes = split(/\s+/,$_);
    
    print "( $phnes[0] \"";
    for ( $k=1 ; $k <= $#phnes; $k++){
	$phnes[$k] =~ tr/[a-z]/[A-Z]/;
	print "$phnes[$k] ";
    }
    print "\" )";
    print "\n";

}
