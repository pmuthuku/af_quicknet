#!/bin/sh

# $Header: /u/drspeech/repos/quicknet2/testdata_qnmultitrn.sh.in,v 1.12 2006/06/24 01:54:18 davidj Exp $
#
# testdata_qnmultitrn.sh.  Generated from testdata_qnmultitrn.sh.in by configure.
#
# This script runs the `qnmultitrn' program, but sets up a useful set of 
# default parameters that use the data files in the `testdata' subdirectory.
# The specificied parameters can be overridden by using
# "param=value" on the command line.

# *** NOTE ***
# This is only an example used for testing - typically ftr1_file and
# ftr2_file will refer to different feature files.  If you only use
# one feature file, set ftr2_file to be null.  Refering to the same feature
# file twice with each reference using a subset of the features will
# result in roughly the same training results as when using just one feature
# file, but the hidden units will be in a different order and hence
# the weight files are not interchangable.

input_file=$1
wts_file=$2
output_file=$3

# Run quicknet
${qnmultifwd:=/home/pmuthuku/toolkits/quicknet-v3_32/binaries/bin/qnmultifwd} \
	ftr1_file=${ftr1_file:="$input_file"} \
	ftr1_format=${ftr1_format:="pfile"} \
	ftr1_width=${ftr1_width:="25"} \
	ftr2_file=${ftr2_file:=""} \
	ftr2_format=${ftr2_format:="pfile"} \
	ftr2_width=${ftr2_width:=0} \
	unary_file=${unary_file:=""} \
	ftr1_norm_file=${ftr1_norm_file:=""} \
	ftr2_norm_file=${ftr2_norm_file:=""} \
	ftr1_ftr_start=${ftr1_ftr_start:=0} \
	ftr1_ftr_count=${ftr1_ftr_count:="25"} \
	ftr2_ftr_start=${ftr2_ftr_start:=0} \
	ftr2_ftr_count=${ftr2_ftr_count:=0} \
	ftr1_delta_order=${ftr1_delta_order:=0} \
	ftr1_delta_win=${ftr1_delta_win:=0} \
	ftr1_norm_mode=${ftr1_norm_mode:=""} \
	ftr1_norm_alpha_m=${ftr1_norm_alpha_m:=0.005} \
	ftr1_norm_alpha_v=${ftr1_norm_alpha_v:=0.005} \
	ftr2_delta_order=${ftr2_delta_order:=0} \
	ftr2_delta_win=${ftr2_delta_win:=0} \
	ftr2_norm_mode=${ftr2_norm_mode:=""} \
	ftr2_norm_alpha_m=${ftr2_norm_alpha_m:=0.005} \
	ftr2_norm_alpha_v=${ftr2_norm_alpha_v:=0.005} \
	ftr1_window_offset=${ftr1_window_offset:=0} \
	ftr1_window_len=${ftr1_window_len:=1} \
	ftr2_window_offset=${ftr2_window_offset:=0} \
	ftr2_window_len=${ftr2_window_len:=1} \
	unary_window_offset=${unary_window_offset:=0} \
	window_extent=${window_extent:=1} \
    fwd_sent_range=${fwd_sent_range:=":"} \
	init_weight_file=${init_weight_file:="$wts_file"} \
	init_weight_format=${init_weight_format:="matlab"} \
	unary_size=${unary_size:=0} \
	mlp_size=${mlp_size:=25,50,12,50,2} \
	mlp_output_type=${mlp_output_type:="sigmoid"} \
	use_pp=${use_pp:=true} \
	use_fe=${use_fe:=true} \
	use_blas=${use_blas:=true} \
	mlp_bunch_size=${mlp_bunch_size:=2048} \
    mlp_threads=${mlp_threads:=16} \
    activation_file=${activation_file:="$output_file"} \
    activation_format=${activation_format:="pfile"} \
	log_file=${log_file:=-} \
	verbose=${verbose:=false} \
    debug=${debug:=0}
