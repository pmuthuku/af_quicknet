/*************************************************************************/
/*                                                                       */
/*                  Language Technologies Institute                      */
/*                     Carnegie Mellon University                        */
/*                        Copyright (c) 2000                             */
/*                        All Rights Reserved.                           */
/*                                                                       */
/*  Permission is hereby granted, free of charge, to use and distribute  */
/*  this software and its documentation without restriction, including   */
/*  without limitation the rights to use, copy, modify, merge, publish,  */
/*  distribute, sublicense, and/or sell copies of this work, and to      */
/*  permit persons to whom this work is furnished to do so, subject to   */
/*  the following conditions:                                            */
/*   1. The code must retain the above copyright notice, this list of    */
/*      conditions and the following disclaimer.                         */
/*   2. Any modifications must be clearly marked as such.                */
/*   3. Original authors' names are not deleted.                         */
/*   4. The authors' names are not used to endorse or promote products   */
/*      derived from this software without specific prior written        */
/*      permission.                                                      */
/*                                                                       */
/*  CARNEGIE MELLON UNIVERSITY AND THE CONTRIBUTORS TO THIS WORK         */
/*  DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING      */
/*  ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT   */
/*  SHALL CARNEGIE MELLON UNIVERSITY NOR THE CONTRIBUTORS BE LIABLE      */
/*  FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES    */
/*  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN   */
/*  AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,          */
/*  ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF       */
/*  THIS SOFTWARE.                                                       */
/*                                                                       */
/*************************************************************************/
/*             Author:  Prasanna Kumar (pmuthuku@cs.cmu.edu)             */
/*               Date:  October 2013                                     */
/*************************************************************************/
/*                                                                       */
/*  Extract Articulatory features corresponding to phone labels          */
/*  from a .lab file and save them to files for each phone               */
/*                                                                       */
/*************************************************************************/
#include <stdio.h>
#include "flite.h"
#include "cst_hrg.h"
#include "cst_track.h"


int main(int argc, char **argv){

  cst_track *af_track;
  cst_relation *r;
  cst_utterance *u;
  cst_item *item;
  int num_frames=0;
  float r_pos;
  int count=0;
  FILE *fp;
  char filenm[100]; // I'm assuming that there are easier ways for malicious people to do exploits than to find machines that use my bad code

  int i,j;

  if (argc != 3){
    fprintf(stderr,"usage: extract_AF_mcep_feats_per_phone LABELFILE AF_TRACK\n\n");
    return 1;
  }


  u = new_utterance();
  r = utt_relation_create(u,"FOO");
  if (relation_load(r,argv[1]) != CST_OK_FORMAT)
    return -1;

  
  af_track = new_track();
  if (cst_track_load_est(af_track,argv[2]) != CST_OK_FORMAT){
    fprintf(stderr, "extract_AF_mcep_feats_per_phone: Can't read file or wrong format\"%s\"\n ",argv[2]);
    return -1;
  }



  float af_avg[af_track->num_channels];
  float af_var[af_track->num_channels];



  //Initialize everything to zero

  for (j = 0; j < af_track->num_channels; j++){

    af_avg[j] = 0;
    af_var[j] = 0;

  }


  
  item = relation_head(r);
  r_pos = val_float(ffeature(item_next(item),"p.end"));

  num_frames =  af_track->num_frames;

  for (i=0; i < num_frames; i++){


    //fprintf(stdout,"Time:%f\tLabel: %s\t%d\n",mcep_track->times[i],item_feat_string(item,"name"),count);
    
    
    for (j=0; j < af_track->num_channels; j++){
      af_avg[j] = af_avg[j] + af_track->frames[i][j]; // Sigma(X)
      af_var[j] = af_var[j] + af_track->frames[i][j]*af_track->frames[i][j]; // Sigma(X^2)
    }

    count++;

    if ( af_track->times[i] >= r_pos ){
      //Compute means and variances from the sums
      for (j=0; j < af_track->num_channels; j++){
        af_avg[j] = af_avg[j]/count; // Sigma(X)/n
        af_var[j] = (af_var[j]/count) - (af_avg[j]*af_avg[j]) ; // Sigma(X^2)/n - (Sigma(X)/n)^2
      }


      // Dump means and variances into appropriate files
      strcpy(filenm,item_feat_string(item,"name")); //Let's hope that there are no phonemes with names longer than 96 characters
      strcat(filenm,".af");
      fp = fopen(filenm,"a");
      for (j=0; j < af_track->num_channels; j++){
        fprintf(fp,"%f\t%f\t", af_avg[j], af_var[j]);
      }
      fprintf(fp,"\n");
      fclose(fp);


      item = item_next(item);
      //Reset counts and means and variances
      count = 0;
      for (j = 0; j < af_track->num_channels; j++){
        af_avg[j] = 0;
        af_var[j] = 0;
      }


      if (!item)
        break;
      else
        r_pos = val_float(ffeature(item_next(item),"p.end"));
    }
    
    //fprintf(stdout,"Time : %f\n\n",val_float(ffeature(item,"p.end")));      
    
  }
  
  
  return 0;
}
