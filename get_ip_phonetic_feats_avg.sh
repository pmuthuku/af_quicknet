#!/bin/bash


PROMPTFILE="$1"

CODE_PATH=/home/pmuthuku/code/AF_quicknet/


if [ "$PROMPTFILE" = "" ]
then
    PROMPTFILE=etc/txt.done.data
fi

if [ -d AF_phonefeats ]
then
 
    echo "
Warning: The feature extraction code is written in a horrible hacky way that will just append the newly extracted feats to previously extracted ones if there are already files in the AF_phonefeats directory 
"
    echo "So, this shell script will fix that problem by deleting the AF_phonefeats directory if it already exists. If you don't want that to happen, kill this script before the following countdown ends and move the directory before rerunning this script
"
    sleep 10s
    
    for i in {1..10}
    do
        echo "11-$i" | bc
        sleep 1s
    done
    echo "Deleting the AF_phonefeats directory..."
    rm -r AF_phonefeats
fi


CG_TMP=jnk_$$

mkdir AF_phonefeats
cd AF_phonefeats

cat ../$PROMPTFILE |
awk '{print $2}' |
while read i
do

    fname=$i
    echo "Getting AF feats per phone for $fname"

    $ESTDIR/bin/ch_track -s 0.005 -otype est_ascii -itype ascii -o $CG_TMP.af ../AF/$fname.af

    $CODE_PATH/extract_AF_mcep_feats_per_phone ../lab/$fname.lab $CG_TMP.af

done

rm -f $CG_TMP.*

#Just for the heck of it, let's do phone statistics on our database
wc -l *.af | sort -nr | sed 's/\.af//g' | tail -n +2 > ../phone_stats

#Let's collate the features together to get a single set of phonetic features for each phoneme

if [ -f all.pfeats ]
then
    rm all.pfeats
fi

for i in *.af
do
    fname=`basename $i .af`
    echo "Getting phonetic features for $fname"
    perl $CODE_PATH/mean_sd.pl $i 1 52 > $CG_TMP.avg
    
    echo "( $fname `cat $CG_TMP.avg` ) " >> all.pfeats

done

cp all.pfeats ../festvox/af_phonetic_features.scm
rm -f $CG_TMP.*