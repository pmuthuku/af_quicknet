#!/bin/bash

# Assumes that the code will be run in the voice directory

CODE_PATH=/home/pmuthuku/code/AF_quicknet/

CG_TMP=jnk_$$
STOP_NUM=$1
mkdir -p phone_trees
cd MCEP_AF_perphone/


# Concatenate all the MCEPs into one file
cat *.mcep | perl -lane 'print "@F[1..$#F]";' | $ESTDIR/bin/ch_track -s 0.005 -itype ascii -otype est_binary -o mcep_all
# Concatenate all AFs into one file and add the vector index for the first column
cat *.af | perl -e '
$count=0;
while(<STDIN>){
   print "$count\t$_";
   $count++;
}' > af_all

cp -p $CODE_PATH/af_mcep_cluster.desc .


$ESTDIR/bin/wagon -track_feats 1-50 -vertex_output mean -desc af_mcep_cluster.desc -data af_all -track mcep_all -output ../phone_trees/STOP$STOP_NUM.tree -stop $STOP_NUM

exit
