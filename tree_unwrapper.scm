(set! tree1 (car (load "/home/pmuthuku/voices/ehmm_AF_exps/ASR_phone_hin/phone_trees/16_ph_subbed.tree" t)))
;(format t "Loaded tree\n")

(define (tree_unwrap tree list1)
  "(tree_unwrap tree string)
Prints out the path taken to reach each leaf of the tree"
  (let ((list2 nil))
  (cond
   ((cdr tree) ;; a question
    (list
     ;(format t "%l\t" (car tree))
     (set! list2 (append list1 '(",")(car tree)))
     (car tree)
     (tree_unwrap (car (cdr tree)) (append list2 '(yes)) )
     (tree_unwrap (car (cdr (cdr tree))) (append list2 '(no)) )))
   (t ;; leaf node
    (list
    (format t "%l\t" (caar (caar tree)))
        (format t "%l\n\n" list1 )  )))))

(tree_unwrap tree1 '())

    
