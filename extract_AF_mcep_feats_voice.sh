#!/bin/bash

PROMPTFILE="$1"

CODE_PATH=/home/pmuthuku/code/AF_quicknet/


if [ "$PROMPTFILE" = "" ]
then
    PROMPTFILE=etc/txt.done.data
fi


if [ ! -d MCEP_AF_perphone ]
then

    mkdir MCEP_AF_perphone

fi


CG_TMP=jnk_$$


cat $PROMPTFILE |
awk '{print $2}' |
while read i
do

    fname=$i
    echo "Getting AF and MCEP feats per phone for $fname"

    cat AF/$fname.af | $ESTDIR/bin/ch_track -s 0.005 -otype est_ascii -itype ascii -o $CG_TMP.af
    
    $CODE_PATH/extract_AF_mcep_feats lab/$fname.lab $CG_TMP.af mcep_static/$fname.mcep MCEP_AF_perphone/$fname.af MCEP_AF_perphone/$fname.mcep

done

rm -f $CG_TMP.*
