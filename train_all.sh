#!/bin/bash

export CODE_PATH=/home/pmuthuku/code/af_quicknet/

if [ ! -d AF_nets ]
then
    mkdir AF_nets
fi

cd cmcep

$CODE_PATH/qn_hard_trainer.sh nn_mcep2paf_train.af1.pfile  nn_mcep2paf_train.af1.wts
$CODE_PATH/qn_hard_trainer.sh nn_mcep2paf_train.af2.pfile  nn_mcep2paf_train.af2.wts
$CODE_PATH/qn_hard_trainer.sh nn_mcep2paf_train.af3.pfile  nn_mcep2paf_train.af3.wts
$CODE_PATH/qn_hard_trainer.sh nn_mcep2paf_train.af4.pfile  nn_mcep2paf_train.af4.wts
$CODE_PATH/qn_hard_trainer.sh nn_mcep2paf_train.af5.pfile  nn_mcep2paf_train.af5.wts
$CODE_PATH/qn_hard_trainer.sh nn_mcep2paf_train.af6.pfile  nn_mcep2paf_train.af6.wts
$CODE_PATH/qn_hard_trainer.sh nn_mcep2paf_train.af7.pfile  nn_mcep2paf_train.af7.wts
$CODE_PATH/qn_hard_trainer.sh nn_mcep2paf_train.af8.pfile  nn_mcep2paf_train.af8.wts
$CODE_PATH/qn_hard_trainer.sh nn_mcep2paf_train.af9.pfile  nn_mcep2paf_train.af9.wts
$CODE_PATH/qn_hard_trainer.sh nn_mcep2paf_train.af10.pfile  nn_mcep2paf_train.af10.wts
$CODE_PATH/qn_hard_trainer.sh nn_mcep2paf_train.af11.pfile  nn_mcep2paf_train.af11.wts
$CODE_PATH/qn_hard_trainer.sh nn_mcep2paf_train.af12.pfile  nn_mcep2paf_train.af12.wts
$CODE_PATH/qn_hard_trainer.sh nn_mcep2paf_train.af13.pfile  nn_mcep2paf_train.af13.wts
$CODE_PATH/qn_hard_trainer.sh nn_mcep2paf_train.af14.pfile  nn_mcep2paf_train.af14.wts
$CODE_PATH/qn_hard_trainer.sh nn_mcep2paf_train.af15.pfile  nn_mcep2paf_train.af15.wts
$CODE_PATH/qn_hard_trainer.sh nn_mcep2paf_train.af16.pfile  nn_mcep2paf_train.af16.wts
$CODE_PATH/qn_hard_trainer.sh nn_mcep2paf_train.af17.pfile  nn_mcep2paf_train.af17.wts
$CODE_PATH/qn_hard_trainer.sh nn_mcep2paf_train.af18.pfile  nn_mcep2paf_train.af18.wts
$CODE_PATH/qn_hard_trainer.sh nn_mcep2paf_train.af19.pfile  nn_mcep2paf_train.af19.wts
$CODE_PATH/qn_hard_trainer.sh nn_mcep2paf_train.af20.pfile  nn_mcep2paf_train.af20.wts
$CODE_PATH/qn_hard_trainer.sh nn_mcep2paf_train.af21.pfile  nn_mcep2paf_train.af21.wts
$CODE_PATH/qn_hard_trainer.sh nn_mcep2paf_train.af22.pfile  nn_mcep2paf_train.af22.wts
$CODE_PATH/qn_hard_trainer.sh nn_mcep2paf_train.af23.pfile  nn_mcep2paf_train.af23.wts
$CODE_PATH/qn_hard_trainer.sh nn_mcep2paf_train.af24.pfile  nn_mcep2paf_train.af24.wts
$CODE_PATH/qn_hard_trainer.sh nn_mcep2paf_train.af25.pfile  nn_mcep2paf_train.af25.wts
$CODE_PATH/qn_hard_trainer.sh nn_mcep2paf_train.af26.pfile  nn_mcep2paf_train.af26.wts

mv *.wts.mat ../AF_nets/
