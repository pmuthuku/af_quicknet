# Makefile for extracting coefficients from AF and MCEPs from label files

CC = gcc
CFLAGS = -g -Wall
INCDIR = /home/pmuthuku/fest_est/jan12/flite/include
LIBDIR = /home/pmuthuku/fest_est/jan12/flite/build/x86_64-linux-gnu/lib/

OTHERFLAGS = -I$(INCDIR) -L $(LIBDIR) -lflite -lm -lasound

all : extract_AF_mcep_feats extract_AF_mcep_feats_per_phone 

extract_AF_mcep_feats : extract_AF_mcep_feats.c
	$(CC) extract_AF_mcep_feats.c $(CFLAGS) $(OTHERFLAGS) -o $@

extract_AF_mcep_feats_per_phone : extract_AF_mcep_feats_per_phone.c
	$(CC) extract_AF_mcep_feats_per_phone.c $(CFLAGS) $(OTHERFLAGS) -o $@