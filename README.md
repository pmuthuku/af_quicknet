# Instructions for building an IP voice:

Details on the IP voice pipeline are available in my [ICASSP 2014 paper](http://www.cs.cmu.edu/~pmuthuku/publications/pdfs/voice_without_labels.pdf).

This code assumes that you have already run a phone recognizer on your
wav files, and the transcriptions are in etc/txt.done.data. The file
looks something like this: 

      ( filename1 "AH BA CA DA ...")
      ( filename2 "XA OW MI ...")
      ...
For my experiments, I obtain transcripts using the technique described in
Sunayana Sitaram's [SSW8 paper](http://www.parlikar.com/files/aup_ssw8_2013_tts.pdf). 

1. Extract AFs using the extract_AFs.sh script. This needs the AF_nets directory and a etc/txt.done.data file. The Neural Nets in the AF_nets directory were trained on the WSJ0 corpus using the QuickNet toolkit. You will have to install QuickNet and pfile_utils to make this code work. You need to add the location of these binaries to your $PATH variable. 

2. Modify cmu_us_*_{tokenizer, phoneset, lexicon} to make sure that the
synthesizer doesn't read each phone as a word i.e. reading "T IH M IY" as "Tee
Eye Emm Eee". Festival uses the tokenizer, phoneset, and lexicon to break down
words into their individual phonemes. However, when you have phoneme transcripts
rather than word transcripts like we do in this task, Festival will think that
the phonemes are actually individual words. So, "T IH M IY" will be pronounced
as "Tee Eye Emm Eee". To fix this, add all of the phonemes to your lexicon and
say that each phoneme is pronounced as itself. If you do not know how to do
this, look at cmu_us_hin_lexicon.scm for examples. You will also need to specify
phonetic features for your decoded phones. If you know the phonetics of these,
add them to cmu_xx_xxx_phoneset.scm; otherwise, set them to be identical to
'pau'. cmu_us_hin_phoneset.scm is in the repo as an example.  

3. Run all the standard voice building steps until the clustering step. (The
clustering step is './bin/do_clustergen cluster etc/txt.done.data.train'. There
may be a parallel argument in the step as well. )

        ./bin/do_build parallel build_prompts
        ./bin/do_build label
        ./bin/do_clustergen parallel build_utts
        rm -rf ehmm/feat
        rm -rf ehmm/binfeat
        ./bin/do_clustergen generate_statenames
        ./bin/do_clustergen generate_filters
        if [ "$SPTKDIR" = "" ]
        then
                ./bin/do_clustergen parallel f0
                ./bin/do_clustergen parallel mcep
                ./bin/do_clustergen parallel voicing
        else
                ./bin/do_clustergen parallel f0_v_sptk
                mv etc/txt.done.data etc/txt.done.data.0
                ./bin/reduce_prompts etc/txt.done.data.0 f0/* >etc/txt.done.data
                ./bin/do_clustergen parallel mcep_sptk
        fi
        ./bin/do_clustergen parallel combine_coeffs_v
        ./bin/traintest etc/txt.done.data
        ./bin/do_clustergen parallel cluster etc/txt.done.data.train


4. Run extract_AF_mcep_feats_voice.sh. This extracts the average AF and MCEP
for each segment in the utterance and puts them in the appropriate file. 

5. sh wagon_cluster1.sh # (for # of stops)
This clusters the AF vectors to produce a single tree
where each leaf of the tree is a phone. Move the tree to a different directory
and rename it appropriately. The reason this file is split into two parts is to
remind people some user intervention is required in this script. The wagon code
basically clusters your AFs so that each cluster corresponds to one
phone. Obviously, you will need to decide on the number of phones you want. The
way to control the number of clusters i.e. the number of phones is to play with
the stop size of the tree.

6. sh wagon_cluster2.sh #(same number as before)
This is a very very hacky piece of
code that assigns names to each of the clusters in our tree.  

7. sh get_new_phonemes.sh #(same number as before) [optional prompt file if not in etc/txt.done.data]
This generates a new_txt.done.data in the etc/
directory. Do a quick check to see if this looks right.  

8. Copy the new_txt.done.data to txt.done.data

9. Delete the ehmm/ directory and the files etc/mapstatenames.awk,
etc/statenames*. 

10. Redo the voice building process upto the combine_coeffs_v step. Make sure to
limit the label steps to 14 iterations of Baum-Welch. This is because the Baum-Welch
code kept falling over whenever I ran it for more iterations.  

11. Run get_ip_phonetic_feats_avg.sh

12. Add for_mcep.desc to festival/clunits/mcep.desc

13. Copy clustergen.scm from my repository to voice_directory/festvox/. (This is
actually kinda dangerous. A safer way of doing this is to just copy the section
of the code below line 1454. Haven't tested this though) 

14. Run the code for clustering and duration modeling. This will take way
longer than you normally expect it to take.  

15. Make sure to turn off mlpg when testing resynthesis. This has caused
problems in the past. (A few months after I stopped working on this code, I
experienced similar problems with MLPG on another project. I found that moving
to the SPTK MLPG code solved the problem. I haven't tried using SPTK code with
this parameterization though.) 