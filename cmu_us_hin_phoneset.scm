;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;                                                                     ;;;
;;;                     Carnegie Mellon University                      ;;;
;;;                  and Alan W Black and Kevin Lenzo                   ;;;
;;;                      Copyright (c) 1998-2000                        ;;;
;;;                        All Rights Reserved.                         ;;;
;;;                                                                     ;;;
;;; Permission is hereby granted, free of charge, to use and distribute ;;;
;;; this software and its documentation without restriction, including  ;;;
;;; without limitation the rights to use, copy, modify, merge, publish, ;;;
;;; distribute, sublicense, and/or sell copies of this work, and to     ;;;
;;; permit persons to whom this work is furnished to do so, subject to  ;;;
;;; the following conditions:                                           ;;;
;;;  1. The code must retain the above copyright notice, this list of   ;;;
;;;     conditions and the following disclaimer.                        ;;;
;;;  2. Any modifications must be clearly marked as such.               ;;;
;;;  3. Original authors' names are not deleted.                        ;;;
;;;  4. The authors' names are not used to endorse or promote products  ;;;
;;;     derived from this software without specific prior written       ;;;
;;;     permission.                                                     ;;;
;;;                                                                     ;;;
;;; CARNEGIE MELLON UNIVERSITY AND THE CONTRIBUTORS TO THIS WORK        ;;;
;;; DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING     ;;;
;;; ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT  ;;;
;;; SHALL CARNEGIE MELLON UNIVERSITY NOR THE CONTRIBUTORS BE LIABLE     ;;;
;;; FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES   ;;;
;;; WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN  ;;;
;;; AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,         ;;;
;;; ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF      ;;;
;;; THIS SOFTWARE.                                                      ;;;
;;;                                                                     ;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; Phonset for US English
;;; uses festival/lib/radio_phones.scm
;;;

;(require 'radio_phones)
(defPhoneSet
  radio
  ;;;  Phone Features
  (;; vowel or consonant
   (vc + -)  
   ;; vowel length: short long dipthong schwa
   (vlng s l d a 0)
   ;; vowel height: high mid low
   (vheight 1 2 3 0)
   ;; vowel frontness: front mid back
   (vfront 1 2 3 0)
   ;; lip rounding
   (vrnd + - 0)
   ;; consonant type: stop fricative affricate nasal lateral approximant
   (ctype s f a n l r 0)
   ;; place of articulation: labial alveolar palatal labio-dental
   ;;                         dental velar glottal
   (cplace l a p b d v g 0)
   ;; consonant voicing
   (cvox + - 0)
   )
  ;; Phone set members
  (
   ;; Note these features were set by awb so they are wrong !!!
   ;; (aa  +   l   3   3   -   0   0   0) ;; father
   ;; (ae  +   s   3   1   -   0   0   0) ;; fat
   ;; (ah  +   s   2   2   -   0   0   0) ;; but
   ;; (ao  +   l   3   3   +   0   0   0) ;; lawn
   ;; (aw  +   d   3   2   -   0   0   0) ;; how
   ;; (ax  +   a   2   2   -   0   0   0) ;; about
   ;; (axr +   a   2   2   -   r   a   +)
   ;; (ay  +   d   3   2   -   0   0   0) ;; hide
   ;; (b   -   0   0   0   0   s   l   +)
   ;; (ch  -   0   0   0   0   a   p   -)
   ;; (d   -   0   0   0   0   s   a   +)
   ;; (dh  -   0   0   0   0   f   d   +)
   ;; (dx  -   a   0   0   0   s   a   +) ;; ??
   ;; (eh  +   s   2   1   -   0   0   0) ;; get
   ;; (el  +   s   0   0   0   l   a   +)
   ;; (em  +   s   0   0   0   n   l   +)
   ;; (en  +   s   0   0   0   n   a   +)
   ;; (er  +   a   2   2   -   r   0   0) ;; always followed by r (er-r == axr)
   ;; (ey  +   d   2   1   -   0   0   0) ;; gate
   ;; (f   -   0   0   0   0   f   b   -)
   ;; (g   -   0   0   0   0   s   v   +)
   ;; (hh  -   0   0   0   0   f   g   -)
   ;; (hv  -   0   0   0   0   f   g   +)
   ;; (ih  +   s   1   1   -   0   0   0) ;; bit
   ;; (iy  +   l   1   1   -   0   0   0) ;; beet
   ;; (jh  -   0   0   0   0   a   p   +)
   ;; (k   -   0   0   0   0   s   v   -)
   ;; (l   -   0   0   0   0   l   a   +)
   ;; (m   -   0   0   0   0   n   l   +)
   ;; (n   -   0   0   0   0   n   a   +)
   ;; (nx  -   0   0   0   0   n   d   +) ;; ???
   ;; (ng  -   0   0   0   0   n   v   +)
   ;; (ow  +   d   2   3   +   0   0   0) ;; lone
   ;; (oy  +   d   2   3   +   0   0   0) ;; toy
   ;; (p   -   0   0   0   0   s   l   -)
   ;; (r   -   0   0   0   0   r   a   +)
   ;; (s   -   0   0   0   0   f   a   -)
   ;; (sh  -   0   0   0   0   f   p   -)
   ;; (t   -   0   0   0   0   s   a   -)
   ;; (th  -   0   0   0   0   f   d   -)
   ;; (uh  +   s   1   3   +   0   0   0) ;; full
   ;; (uw  +   l   1   3   +   0   0   0) ;; fool
   ;; (v   -   0   0   0   0   f   b   +)
   ;; (w   -   0   0   0   0   r   l   +)
   ;; (y   -   0   0   0   0   r   p   +)
   ;; (z   -   0   0   0   0   f   a   +)
   ;; (zh  -   0   0   0   0   f   p   +)

      ;; Need to do some experiments with a crippled phoneset. So, replacing the above with the following:
   (aa -  0   0   0   0   0   0   -)
   (ae -  0   0   0   0   0   0   -)
   (ah -  0   0   0   0   0   0   -)
   (ao -  0   0   0   0   0   0   -)
   (aw -  0   0   0   0   0   0   -)
   (ax -  0   0   0   0   0   0   -)
   (axr -  0   0   0   0   0   0   -)
   (ay -  0   0   0   0   0   0   -)
   (b -  0   0   0   0   0   0   -)
   (ch -  0   0   0   0   0   0   -)
   (d -  0   0   0   0   0   0   -)
   (dh -  0   0   0   0   0   0   -)
   (dx -  0   0   0   0   0   0   -)
   (eh -  0   0   0   0   0   0   -)
   (el -  0   0   0   0   0   0   -)
   (em -  0   0   0   0   0   0   -)
   (en -  0   0   0   0   0   0   -)
   (er -  0   0   0   0   0   0   -)
   (ey -  0   0   0   0   0   0   -)
   (f -  0   0   0   0   0   0   -)
   (g -  0   0   0   0   0   0   -)
   (hh -  0   0   0   0   0   0   -)
   (hv -  0   0   0   0   0   0   -)
   (ih -  0   0   0   0   0   0   -)
   (iy -  0   0   0   0   0   0   -)
   (jh -  0   0   0   0   0   0   -)
   (k -  0   0   0   0   0   0   -)
   (l -  0   0   0   0   0   0   -)
   (m -  0   0   0   0   0   0   -)
   (n -  0   0   0   0   0   0   -)
   (nx -  0   0   0   0   0   0   -)
   (ng -  0   0   0   0   0   0   -)
   (ow -  0   0   0   0   0   0   -)
   (oy -  0   0   0   0   0   0   -)
   (p -  0   0   0   0   0   0   -)
   (r -  0   0   0   0   0   0   -)
   (s -  0   0   0   0   0   0   -)
   (sh -  0   0   0   0   0   0   -)
   (t -  0   0   0   0   0   0   -)
   (th -  0   0   0   0   0   0   -)
   (uh -  0   0   0   0   0   0   -)
   (uw -  0   0   0   0   0   0   -)
   (v -  0   0   0   0   0   0   -)
   (w -  0   0   0   0   0   0   -)
   (y -  0   0   0   0   0   0   -)
   (z -  0   0   0   0   0   0   -)
   (zh -  0   0   0   0   0   0   -)


   (ip1 -   0   0   0   0   0   0   -)
   (ip2 -   0   0   0   0   0   0   -)
   (ip3 -   0   0   0   0   0   0   -)
   (ip4 -   0   0   0   0   0   0   -)
   (ip5 -   0   0   0   0   0   0   -)
   (ip6 -   0   0   0   0   0   0   -)
   (ip7 -   0   0   0   0   0   0   -)
   (ip8 -   0   0   0   0   0   0   -)
   (ip9 -   0   0   0   0   0   0   -)
   (ip10 -   0   0   0   0   0   0   -)
   (ip11 -   0   0   0   0   0   0   -)
   (ip12 -   0   0   0   0   0   0   -)
   (ip13 -   0   0   0   0   0   0   -)
   (ip14 -   0   0   0   0   0   0   -)
   (ip15 -   0   0   0   0   0   0   -)
   (ip16 -   0   0   0   0   0   0   -)
   (ip17 -   0   0   0   0   0   0   -)
   (ip18 -   0   0   0   0   0   0   -)
   (ip19 -   0   0   0   0   0   0   -)
   (ip20 -   0   0   0   0   0   0   -)
   (ip21 -   0   0   0   0   0   0   -)
   (ip22 -   0   0   0   0   0   0   -)
   (ip23 -   0   0   0   0   0   0   -)
   (ip24 -   0   0   0   0   0   0   -)
   (ip25 -   0   0   0   0   0   0   -)
   (ip26 -   0   0   0   0   0   0   -)
   (ip27 -   0   0   0   0   0   0   -)
   (ip28 -   0   0   0   0   0   0   -)
   (ip29 -   0   0   0   0   0   0   -)
   (ip30 -   0   0   0   0   0   0   -)
   (ip31 -   0   0   0   0   0   0   -)
   (ip32 -   0   0   0   0   0   0   -)
   (ip33 -   0   0   0   0   0   0   -)
   (ip34 -   0   0   0   0   0   0   -)
   (ip35 -   0   0   0   0   0   0   -)
   (ip36 -   0   0   0   0   0   0   -)
   (ip37 -   0   0   0   0   0   0   -)
   (ip38 -   0   0   0   0   0   0   -)
   (ip39 -   0   0   0   0   0   0   -)
   (ip40 -   0   0   0   0   0   0   -)
   (ip41 -   0   0   0   0   0   0   -)
   (ip42 -   0   0   0   0   0   0   -)
   (ip43 -   0   0   0   0   0   0   -)
   (ip44 -   0   0   0   0   0   0   -)
   (ip45 -   0   0   0   0   0   0   -)
   (ip46 -   0   0   0   0   0   0   -)
   (ip47 -   0   0   0   0   0   0   -)
   (ip48 -   0   0   0   0   0   0   -)
   (ip49 -   0   0   0   0   0   0   -)
   (ip50 -   0   0   0   0   0   0   -)
   (ip51 -   0   0   0   0   0   0   -)
   (ip52 -   0   0   0   0   0   0   -)
   (ip53 -   0   0   0   0   0   0   -)
   (ip54 -   0   0   0   0   0   0   -)
   (ip55 -   0   0   0   0   0   0   -)
   (ip56 -   0   0   0   0   0   0   -)
   (ip57 -   0   0   0   0   0   0   -)
   (ip58 -   0   0   0   0   0   0   -)
   (ip59 -   0   0   0   0   0   0   -)
   (ip60 -   0   0   0   0   0   0   -)
   (ip61 -   0   0   0   0   0   0   -)
   (ip62 -   0   0   0   0   0   0   -)
   (ip63 -   0   0   0   0   0   0   -)
   (ip64 -   0   0   0   0   0   0   -)
   (ip65 -   0   0   0   0   0   0   -)
   (ip66 -   0   0   0   0   0   0   -)
   (ip67 -   0   0   0   0   0   0   -)
   (ip68 -   0   0   0   0   0   0   -)
   (ip69 -   0   0   0   0   0   0   -)
   (ip70 -   0   0   0   0   0   0   -)
   (ip71 -   0   0   0   0   0   0   -)
   (ip72 -   0   0   0   0   0   0   -)
   (ip73 -   0   0   0   0   0   0   -)
   (ip74 -   0   0   0   0   0   0   -)
   (ip75 -   0   0   0   0   0   0   -)
   (ip76 -   0   0   0   0   0   0   -)
   (ip77 -   0   0   0   0   0   0   -)
   (ip78 -   0   0   0   0   0   0   -)
   (ip79 -   0   0   0   0   0   0   -)
   (ip80 -   0   0   0   0   0   0   -)
   (ip81 -   0   0   0   0   0   0   -)
   (ip82 -   0   0   0   0   0   0   -)
   (ip83 -   0   0   0   0   0   0   -)
   (ip84 -   0   0   0   0   0   0   -)
   (ip85 -   0   0   0   0   0   0   -)
   (ip86 -   0   0   0   0   0   0   -)
   (ip87 -   0   0   0   0   0   0   -)
   (ip88 -   0   0   0   0   0   0   -)
   (ip89 -   0   0   0   0   0   0   -)
   (ip90 -   0   0   0   0   0   0   -)
   (ip91 -   0   0   0   0   0   0   -)
   (ip92 -   0   0   0   0   0   0   -)
   (ip93 -   0   0   0   0   0   0   -)
   (ip94 -   0   0   0   0   0   0   -)
   (ip95 -   0   0   0   0   0   0   -)
   (ip96 -   0   0   0   0   0   0   -)
   (ip97 -   0   0   0   0   0   0   -)
   (ip98 -   0   0   0   0   0   0   -)
   (ip99 -   0   0   0   0   0   0   -)
   (ip100 -   0   0   0   0   0   0   -)
   (ip101 -   0   0   0   0   0   0   -)
   (ip102 -   0   0   0   0   0   0   -)
   (ip103 -   0   0   0   0   0   0   -)
   (ip104 -   0   0   0   0   0   0   -)
   (ip105 -   0   0   0   0   0   0   -)
   (ip106 -   0   0   0   0   0   0   -)
   (ip107 -   0   0   0   0   0   0   -)
   (ip108 -   0   0   0   0   0   0   -)
   (ip109 -   0   0   0   0   0   0   -)
   (ip110 -   0   0   0   0   0   0   -)
   (ip111 -   0   0   0   0   0   0   -)
   (ip112 -   0   0   0   0   0   0   -)
   (ip113 -   0   0   0   0   0   0   -)
   (ip114 -   0   0   0   0   0   0   -)
   (ip115 -   0   0   0   0   0   0   -)
   (ip116 -   0   0   0   0   0   0   -)
   (ip117 -   0   0   0   0   0   0   -)
   (ip118 -   0   0   0   0   0   0   -)
   (ip119 -   0   0   0   0   0   0   -)
   (ip120 -   0   0   0   0   0   0   -)
   (ip121 -   0   0   0   0   0   0   -)
   (ip122 -   0   0   0   0   0   0   -)
   (ip123 -   0   0   0   0   0   0   -)
   (ip124 -   0   0   0   0   0   0   -)
   (ip125 -   0   0   0   0   0   0   -)
   (ip126 -   0   0   0   0   0   0   -)
   (ip127 -   0   0   0   0   0   0   -)
   (ip128 -   0   0   0   0   0   0   -)
   (ip129 -   0   0   0   0   0   0   -)
   (ip130 -   0   0   0   0   0   0   -)
   (ip131 -   0   0   0   0   0   0   -)
   (ip132 -   0   0   0   0   0   0   -)
   (ip133 -   0   0   0   0   0   0   -)
   (ip134 -   0   0   0   0   0   0   -)
   (ip135 -   0   0   0   0   0   0   -)
   (ip136 -   0   0   0   0   0   0   -)
   (ip137 -   0   0   0   0   0   0   -)
   (ip138 -   0   0   0   0   0   0   -)
   (ip139 -   0   0   0   0   0   0   -)
   (ip140 -   0   0   0   0   0   0   -)
   (ip141 -   0   0   0   0   0   0   -)
   (ip142 -   0   0   0   0   0   0   -)
   (ip143 -   0   0   0   0   0   0   -)
   (ip144 -   0   0   0   0   0   0   -)
   (ip145 -   0   0   0   0   0   0   -)
   (ip146 -   0   0   0   0   0   0   -)
   (ip147 -   0   0   0   0   0   0   -)
   (ip148 -   0   0   0   0   0   0   -)
   (ip149 -   0   0   0   0   0   0   -)
   (ip150 -   0   0   0   0   0   0   -)
   (ip151 -   0   0   0   0   0   0   -)
   (ip152 -   0   0   0   0   0   0   -)
   (ip153 -   0   0   0   0   0   0   -)
   (ip154 -   0   0   0   0   0   0   -)
   (ip155 -   0   0   0   0   0   0   -)
   (ip156 -   0   0   0   0   0   0   -)
   (ip157 -   0   0   0   0   0   0   -)
   (ip158 -   0   0   0   0   0   0   -)
   (ip159 -   0   0   0   0   0   0   -)
   (ip160 -   0   0   0   0   0   0   -)
   (ip161 -   0   0   0   0   0   0   -)
   (ip162 -   0   0   0   0   0   0   -)
   (ip163 -   0   0   0   0   0   0   -)
   (ip164 -   0   0   0   0   0   0   -)
   (ip165 -   0   0   0   0   0   0   -)
   (ip166 -   0   0   0   0   0   0   -)
   (ip167 -   0   0   0   0   0   0   -)
   (ip168 -   0   0   0   0   0   0   -)
   (ip169 -   0   0   0   0   0   0   -)
   (ip170 -   0   0   0   0   0   0   -)
   (ip171 -   0   0   0   0   0   0   -)
   (ip172 -   0   0   0   0   0   0   -)
   (ip173 -   0   0   0   0   0   0   -)
   (ip174 -   0   0   0   0   0   0   -)
   (ip175 -   0   0   0   0   0   0   -)
   (ip176 -   0   0   0   0   0   0   -)
   (ip177 -   0   0   0   0   0   0   -)
   (ip178 -   0   0   0   0   0   0   -)
   (ip179 -   0   0   0   0   0   0   -)
   (ip180 -   0   0   0   0   0   0   -)
   (ip181 -   0   0   0   0   0   0   -)
   (ip182 -   0   0   0   0   0   0   -)
   (ip183 -   0   0   0   0   0   0   -)
   (ip184 -   0   0   0   0   0   0   -)
   (ip185 -   0   0   0   0   0   0   -)
   (ip186 -   0   0   0   0   0   0   -)
   (ip187 -   0   0   0   0   0   0   -)
   (ip188 -   0   0   0   0   0   0   -)
   (ip189 -   0   0   0   0   0   0   -)
   (ip190 -   0   0   0   0   0   0   -)
   (ip191 -   0   0   0   0   0   0   -)
   (ip192 -   0   0   0   0   0   0   -)
   (ip193 -   0   0   0   0   0   0   -)
   (ip194 -   0   0   0   0   0   0   -)
   (ip195 -   0   0   0   0   0   0   -)
   (ip196 -   0   0   0   0   0   0   -)
   (ip197 -   0   0   0   0   0   0   -)
   (ip198 -   0   0   0   0   0   0   -)
   (ip199 -   0   0   0   0   0   0   -)
   (ip200 -   0   0   0   0   0   0   -)
   (ip201 -   0   0   0   0   0   0   -)
   (ip202 -   0   0   0   0   0   0   -)
   (ip203 -   0   0   0   0   0   0   -)
   (ip204 -   0   0   0   0   0   0   -)
   (ip205 -   0   0   0   0   0   0   -)
   (ip206 -   0   0   0   0   0   0   -)
   (ip207 -   0   0   0   0   0   0   -)
   (ip208 -   0   0   0   0   0   0   -)
   (ip209 -   0   0   0   0   0   0   -)
   (ip210 -   0   0   0   0   0   0   -)


   (pau -   0   0   0   0   0   0   -)
   (h#  -   0   0   0   0   0   0   -)
   (brth -   0   0   0   0   0   0   -)
  )
)

(PhoneSet.silences '(pau h# brth))


(define (cmu_us_hin::select_phoneset)
  "(cmu_us_hin::select_phoneset)
Set up phone set for US English."
  (Parameter.set 'PhoneSet 'radio)
  (PhoneSet.select 'radio)
)

(define (cmu_us_hin::reset_phoneset)
  "(cmu_us_hin::reset_phoneset)
Reset phone set for US English."
  t
)

(provide 'cmu_us_hin_phoneset)
