#!/bin/perl 

use warnings;
use strict;

#open(Input,"<$ARGV[0]");
my $sent_num=0;
my $col_num=0;

while(<STDIN>){
    chomp;
    print "$sent_num $col_num  $_\n";
    $col_num++;
    if($col_num%500==0){
	$sent_num++;
	$col_num=0;
    }
}
#close(Input);
