#!/bin/perl

use warnings;
use strict;

if($#ARGV < 0){
    print STDERR "Usage:\n1:File1\n2:Starting column number\n3:Ending column number\n";
    exit;
}

open(Inp,"<$ARGV[0]") || die ("Input file not found");;
my $x1=$ARGV[1];
my $x2=$ARGV[2];
my $range=$x2-$x1+1;
my @sum=( (0) x $range);
my @sum_sq=( (0) x $range);
my @lin1;
my $i=0;
my $j=0;
my $k=0;
my $count=0;
my $me_of_me=0;
my $me_of_sd=0;

if($range < 0){
    print STDERR "Ending column number < Starting column number :( \n";
    exit;
}

#Mean Calculation
while(<Inp>){
    chomp;
    s/^\s+//;
    @lin1=split(/\s+/,$_);
    if(!defined($lin1[$x2-1])){
	print STDERR "Stated range exceeds dimensions\n";
	exit;
    }
    for($i=($x1-1),$j=0;$i < $x2; $i++,$j++){
	$sum[$j]=$sum[$j]+$lin1[$i];
    }
    $count++;
}

for($k=0; $k < $range; $k++){
    $sum[$k]=$sum[$k]/$count;
    printf("%f ",$sum[$k]);
}
print "\n";
close(Inp);

# open(Inp,"<$ARGV[0]");
# #Standard Deviation calculation
# while(<Inp>){
#     chomp;
#     s/^\s+//;
#     @lin1=split(/\s+/,$_);
#     for($i=($x1-1),$j=0; $i < $x2; $i++,$j++){
# 	$sum_sq[$j]=$sum_sq[$j]+(($lin1[$i]-$sum[$j])**2);
#     }
# }

# for($k=0; $k < $range; $k++){
#     $sum_sq[$k]=$sum_sq[$k]/$count;
#     $sum_sq[$k]=sqrt($sum_sq[$k]);
#     print "$sum_sq[$k] ";
# }
# print "\n";
# close(Inp);

# #Mean of means calculation
# for($k=0; $k < $range; $k++){
#     $me_of_me=$me_of_me+$sum[$k];
#     $me_of_sd=$me_of_sd+$sum_sq[$k];
# }
# $me_of_me=$me_of_me/$range;
# $me_of_sd=$me_of_sd/$range;

# print "$me_of_me\n";
# print "$me_of_sd\n";
