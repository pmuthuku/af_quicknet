#!/bin/bash

PROMPTFILE="$1"

if [ ! -d AF  ]
then
    mkdir AF
fi

if [ ! -d mcep_static ]
then
    mkdir mcep_static
fi


if [ "$PROMPTFILE" = "" ]
then
    PROMPTFILE=etc/txt.done.data
fi

CG_TMP=jnk_$$

x=0

CODE_PATH=/home/pmuthuku/code/AF_quicknet/

#Extract Static mceps first
./bin/do_clustergen mcep_static

cat $PROMPTFILE |
awk '{print $2}' |
while read i
do
    
    fname=$i
    echo "Extracting AFs for $fname"
    
    #Prepare Input file
    #x=`echo $[x+1]`
    $ESTDIR/bin/ch_track -otype ascii mcep_static/$fname.mcep |
    awk '{if (NR == 1) { print $0; } print $0}' | 
    python $CODE_PATH/pfile_preparer_opts.py 0 |
    pfile_create -i - -f 25 -o $CG_TMP.mcep.pfile
    
    #Extract AFs from Melceps
    $CODE_PATH/qnfwds.sh $CG_TMP.mcep.pfile AF_nets/nn_mcep2paf_train.af1.wts.mat $CG_TMP.af1.pfile
    pfile_print -i $CG_TMP.af1.pfile | tail -n +2 | awk '{print $4}' > $CG_TMP.af1
    
    $CODE_PATH/qnfwds.sh $CG_TMP.mcep.pfile AF_nets/nn_mcep2paf_train.af2.wts.mat $CG_TMP.af2.pfile
    pfile_print -i $CG_TMP.af2.pfile | tail -n +2 | awk '{print $4}' > $CG_TMP.af2
    
    $CODE_PATH/qnfwds.sh $CG_TMP.mcep.pfile AF_nets/nn_mcep2paf_train.af3.wts.mat $CG_TMP.af3.pfile
    pfile_print -i $CG_TMP.af3.pfile | tail -n +2 | awk '{print $4}' > $CG_TMP.af3
    
    $CODE_PATH/qnfwds.sh $CG_TMP.mcep.pfile AF_nets/nn_mcep2paf_train.af4.wts.mat $CG_TMP.af4.pfile
    pfile_print -i $CG_TMP.af4.pfile | tail -n +2 | awk '{print $4}' > $CG_TMP.af4
    
    $CODE_PATH/qnfwds.sh $CG_TMP.mcep.pfile AF_nets/nn_mcep2paf_train.af5.wts.mat $CG_TMP.af5.pfile
    pfile_print -i $CG_TMP.af5.pfile | tail -n +2 | awk '{print $4}' > $CG_TMP.af5
    
    $CODE_PATH/qnfwds.sh $CG_TMP.mcep.pfile AF_nets/nn_mcep2paf_train.af6.wts.mat $CG_TMP.af6.pfile
    pfile_print -i $CG_TMP.af6.pfile | tail -n +2 | awk '{print $4}' > $CG_TMP.af6
    
    $CODE_PATH/qnfwds.sh $CG_TMP.mcep.pfile AF_nets/nn_mcep2paf_train.af7.wts.mat $CG_TMP.af7.pfile
    pfile_print -i $CG_TMP.af7.pfile | tail -n +2 | awk '{print $4}' > $CG_TMP.af7
    
    $CODE_PATH/qnfwds.sh $CG_TMP.mcep.pfile AF_nets/nn_mcep2paf_train.af8.wts.mat $CG_TMP.af8.pfile
    pfile_print -i $CG_TMP.af8.pfile | tail -n +2 | awk '{print $4}' > $CG_TMP.af8
    
    $CODE_PATH/qnfwds.sh $CG_TMP.mcep.pfile AF_nets/nn_mcep2paf_train.af9.wts.mat $CG_TMP.af9.pfile
    pfile_print -i $CG_TMP.af9.pfile | tail -n +2 | awk '{print $4}' > $CG_TMP.af9
    
    $CODE_PATH/qnfwds.sh $CG_TMP.mcep.pfile AF_nets/nn_mcep2paf_train.af10.wts.mat $CG_TMP.af10.pfile
    pfile_print -i $CG_TMP.af10.pfile | tail -n +2 | awk '{print $4}' > $CG_TMP.af10
    
    $CODE_PATH/qnfwds.sh $CG_TMP.mcep.pfile AF_nets/nn_mcep2paf_train.af11.wts.mat $CG_TMP.af11.pfile
    pfile_print -i $CG_TMP.af11.pfile | tail -n +2 | awk '{print $4}' > $CG_TMP.af11
    
    $CODE_PATH/qnfwds.sh $CG_TMP.mcep.pfile AF_nets/nn_mcep2paf_train.af12.wts.mat $CG_TMP.af12.pfile
    pfile_print -i $CG_TMP.af12.pfile | tail -n +2 | awk '{print $4}' > $CG_TMP.af12
    
    $CODE_PATH/qnfwds.sh $CG_TMP.mcep.pfile AF_nets/nn_mcep2paf_train.af13.wts.mat $CG_TMP.af13.pfile
    pfile_print -i $CG_TMP.af13.pfile | tail -n +2 | awk '{print $4}' > $CG_TMP.af13
    
    $CODE_PATH/qnfwds.sh $CG_TMP.mcep.pfile AF_nets/nn_mcep2paf_train.af14.wts.mat $CG_TMP.af14.pfile
    pfile_print -i $CG_TMP.af14.pfile | tail -n +2 | awk '{print $4}' > $CG_TMP.af14
    
    $CODE_PATH/qnfwds.sh $CG_TMP.mcep.pfile AF_nets/nn_mcep2paf_train.af15.wts.mat $CG_TMP.af15.pfile
    pfile_print -i $CG_TMP.af15.pfile | tail -n +2 | awk '{print $4}' > $CG_TMP.af15
    
    $CODE_PATH/qnfwds.sh $CG_TMP.mcep.pfile AF_nets/nn_mcep2paf_train.af16.wts.mat $CG_TMP.af16.pfile
    pfile_print -i $CG_TMP.af16.pfile | tail -n +2 | awk '{print $4}' > $CG_TMP.af16
    
    $CODE_PATH/qnfwds.sh $CG_TMP.mcep.pfile AF_nets/nn_mcep2paf_train.af17.wts.mat $CG_TMP.af17.pfile
    pfile_print -i $CG_TMP.af17.pfile | tail -n +2 | awk '{print $4}' > $CG_TMP.af17
    
    $CODE_PATH/qnfwds.sh $CG_TMP.mcep.pfile AF_nets/nn_mcep2paf_train.af18.wts.mat $CG_TMP.af18.pfile
    pfile_print -i $CG_TMP.af18.pfile | tail -n +2 | awk '{print $4}' > $CG_TMP.af18
    
    $CODE_PATH/qnfwds.sh $CG_TMP.mcep.pfile AF_nets/nn_mcep2paf_train.af19.wts.mat $CG_TMP.af19.pfile
    pfile_print -i $CG_TMP.af19.pfile | tail -n +2 | awk '{print $4}' > $CG_TMP.af19
    
    $CODE_PATH/qnfwds.sh $CG_TMP.mcep.pfile AF_nets/nn_mcep2paf_train.af20.wts.mat $CG_TMP.af20.pfile
    pfile_print -i $CG_TMP.af20.pfile | tail -n +2 | awk '{print $4}' > $CG_TMP.af20
    
    $CODE_PATH/qnfwds.sh $CG_TMP.mcep.pfile AF_nets/nn_mcep2paf_train.af21.wts.mat $CG_TMP.af21.pfile
    pfile_print -i $CG_TMP.af21.pfile | tail -n +2 | awk '{print $4}' > $CG_TMP.af21
    
    $CODE_PATH/qnfwds.sh $CG_TMP.mcep.pfile AF_nets/nn_mcep2paf_train.af22.wts.mat $CG_TMP.af22.pfile
    pfile_print -i $CG_TMP.af22.pfile | tail -n +2 | awk '{print $4}' > $CG_TMP.af22
    
    $CODE_PATH/qnfwds.sh $CG_TMP.mcep.pfile AF_nets/nn_mcep2paf_train.af23.wts.mat $CG_TMP.af23.pfile
    pfile_print -i $CG_TMP.af23.pfile | tail -n +2 | awk '{print $4}' > $CG_TMP.af23
    
    $CODE_PATH/qnfwds.sh $CG_TMP.mcep.pfile AF_nets/nn_mcep2paf_train.af24.wts.mat $CG_TMP.af24.pfile
    pfile_print -i $CG_TMP.af24.pfile | tail -n +2 | awk '{print $4}' > $CG_TMP.af24
    
    $CODE_PATH/qnfwds.sh $CG_TMP.mcep.pfile AF_nets/nn_mcep2paf_train.af25.wts.mat $CG_TMP.af25.pfile
    pfile_print -i $CG_TMP.af25.pfile | tail -n +2 | awk '{print $4}' > $CG_TMP.af25
    
    $CODE_PATH/qnfwds.sh $CG_TMP.mcep.pfile AF_nets/nn_mcep2paf_train.af26.wts.mat $CG_TMP.af26.pfile
    pfile_print -i $CG_TMP.af26.pfile | tail -n +2 | awk '{print $4}' > $CG_TMP.af26
    
    paste $CG_TMP.af1 $CG_TMP.af2 $CG_TMP.af3 $CG_TMP.af4 $CG_TMP.af5 $CG_TMP.af6 $CG_TMP.af7 $CG_TMP.af8 $CG_TMP.af9 $CG_TMP.af10 $CG_TMP.af11 $CG_TMP.af12 $CG_TMP.af13 $CG_TMP.af14 $CG_TMP.af15 $CG_TMP.af16 $CG_TMP.af17 $CG_TMP.af18 $CG_TMP.af19 $CG_TMP.af20 $CG_TMP.af21 $CG_TMP.af22 $CG_TMP.af23 $CG_TMP.af24 $CG_TMP.af25 $CG_TMP.af26 > AF/$fname.af

done

rm -f $CG_TMP.*