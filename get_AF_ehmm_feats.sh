#!/bin/bash

PROMPTFILE="$1"

if [ "$PROMPTFILE" = "" ]
then
    PROMPTFILE=etc/txt.done.data
fi

CG_TMP=jnk_$$
export EHMMDIR=$FESTVOXDIR/src/ehmm

num_cpus=6

if [ ! -d ehmm/binfeat ]
then
    mkdir ehmm/binfeat
fi


cat $PROMPTFILE |
awk '{print $2}' |
while read i
do

    fname=$i
    echo "Get AF feats for EHMM $fname"
    
    #Input file
    wc -l AF/$fname.af | awk '{print $1}' > $CG_TMP.af
    awk '{print NF}' AF/$fname.af | uniq >>  $CG_TMP.af
    cat AF/$fname.af >> $CG_TMP.af

    #Convert Features to Binary
    $EHMMDIR/bin/ConvertFeatsFileToBinaryFormat $CG_TMP.af ehmm/binfeat/$fname.ft

done

# Normalize and Scale the feats
$EHMMDIR/bin/ScaleBinaryFeats ehmm/etc/mywavelist 1 $num_cpus
    # 4 => Scaling Factor
    # Last => Number of threads to spawn


perl $EHMMDIR/bin/seqproc.pl ehmm/etc/txt.phseq.data ehmm/etc/ph_list 2 2 26
 #Last params: no.gaussains, no. of connections, feature_dimension

rm -f $CG_TMP.*