#!/bin/bash

# # The lines below will have substitution made in the configure process
# : ${name:="qns-1"}
# : ${file:="slt"}
# : ${pfile:=$file.pfile}
# : ${pfileTarget:=$file"-labels-normed.pfile"}
# : ${normfile:=$file.norm}
# :    ${cv_sent_range:=`pfile_info $pfile|tail -1|awk '{printf (":%d\n", int($1/10)-1)}'`}
# : ${train_sent_range:=`pfile_info $pfile|tail -1|awk '{printf ("%d:\n", int($1/10))}'`}
# :  ${num_input_nodes:=`pfile_info $pfile|tail -1|awk '{print $7}'`}
# #: ${num_output_nodes:=`awk ' {print NF } ' 0|sort -u`}
# : ${num_output_nodes:=13}
# : ${num_middle_nodes:=130}

# # Run normalization
# qnnorm norm_ftrfile=${pfile} output_normfile=${normfile}
# pfile_norm -i ${file}-labels.pfile -o ${pfileTarget} -mean 0.5 -std 0.22

target_file=$1
output_wt_file=$2


# Run quicknet
${qnmultitrn:=qnmultitrn} \
        ftr1_file=${ftr1_file:="/home2/pmuthuku/voices/dummy_rms/cmcep/input_data_mcep.pfile"} \
        ftr1_format=${ftr1_format:="pfile"} \
        ftr1_width=${ftr1_width:="25"} \
        ftr2_file=${ftr2_file:=""} \
        ftr2_format=${ftr2_format:="pfile"} \
        ftr2_width=${ftr2_width:=0} \
        unary_file=${unary_file:=""} \
        ftr1_norm_file=${ftr1_norm_file:=""} \
        ftr2_norm_file=${ftr2_norm_file:=""} \
        ftr1_ftr_start=${ftr1_ftr_start:=0} \
        ftr1_ftr_count=${ftr1_ftr_count:="25"} \
        ftr2_ftr_start=${ftr2_ftr_start:=0} \
        ftr2_ftr_count=${ftr2_ftr_count:=0} \
        ftr1_delta_order=${ftr1_delta_order:=0} \
        ftr1_delta_win=${ftr1_delta_win:=0} \
        ftr1_norm_mode=${ftr1_norm_mode:=""} \
        ftr1_norm_alpha_m=${ftr1_norm_alpha_m:=0.005} \
        ftr1_norm_alpha_v=${ftr1_norm_alpha_v:=0.005} \
        ftr2_delta_order=${ftr2_delta_order:=0} \
        ftr2_delta_win=${ftr2_delta_win:=0} \
        ftr2_norm_mode=${ftr2_norm_mode:=""} \
        ftr2_norm_alpha_m=${ftr2_norm_alpha_m:=0.005} \
        ftr2_norm_alpha_v=${ftr2_norm_alpha_v:=0.005} \
        ftr1_window_offset=${ftr1_window_offset:=0} \
        ftr1_window_len=${ftr1_window_len:=1} \
        ftr2_window_offset=${ftr2_window_offset:=0} \
        ftr2_window_len=${ftr2_window_len:=1} \
        unary_window_offset=${unary_window_offset:=0} \
        hardtarget_file=${hardtarget_file:="$target_file"} \
        hardtarget_format=${hardtarget_format:="pfile"} \
        hardtarget_window_offset=${hardtarget_window_offset:=0} \
        hardtarget_lastlab_reject=${hardtarget_lastlab_reject:=0} \
        softtarget_file=${softtarget_file:=""} \
        softtarget_format=${softtarget_format:=""} \
        softtarget_width=${softtarget_width:="0"} \
        softtarget_window_offset=${softtarget_window_offset:=0} \
        window_extent=${window_extent:=1} \
        train_cache_frames=${train_cache_frames:=100000} \
        train_cache_seed=${train_cache_seed:=0} \
        train_sent_range=${train_sent_range:=":225"} \
        cv_sent_range=${cv_sent_range:="226:"} \
        init_weight_file=${init_weight_file:=""} \
        init_weight_format=${init_weight_format:="matlab"} \
        out_weight_file=${out_weight_file:="$output_wt_file.mat"} \
        out_weight_format=${out_weight_format:="matlab"} \
        log_weight_file=${log_weight_file:="$output_wt_file.log.mat"} \
        log_weight_format=${log_weight_format:="matlab"} \
        ckpt_weight_file=${ckpt_weight_file:="$output_wt_file.ckpt.mat"} \
        ckpt_weight_format=${ckpt_weight_format:="matlab"} \
        ckpt_hours=${ckpt_hours:=0} \
        init_random_bias_min=${init_random_bias_min:=-0.1,-4.1} \
        init_random_bias_max=${init_random_bias_max:=0.1,-3.9} \
        init_random_weight_min=${init_random_weight_min:=-0.1} \
        init_random_weight_max=${init_random_weight_max:=0.1} \
        init_random_seed=${init_random_seed:=0} \
        learnrate_schedule=${learnrate_schedule:=newbob} \
        learnrate_vals=${learnrate_vals:=0.008} \
        learnrate_epochs=${learnrate_epochs:=9999} \
        learnrate_scale=${learnrate_scale:=0.03} \
        unary_size=${unary_size:=0} \
        mlp_size=${mlp_size:=25,50,12,50,2} \
        mlp_output_type=${mlp_output_type:="sigmoid"} \
        mlp_lrmultiplier=${mlp_lrmultiplier:=1.0} \
        use_pp=${use_pp:=true} \
        use_fe=${use_fe:=true} \
        use_blas=${use_blas:=true} \
        mlp_bunch_size=${mlp_bunch_size:=2048} \
        mlp_threads=${mlp_threads:=16} \
        log_file=${log_file:=-} \
        verbose=${verbose:=false} \
        debug=${debug:=0}
