#!/bin/bash

# Assumes that the code will be run in the voice directory

CODE_PATH=/home/pmuthuku/code/AF_quicknet/

CG_TMP=jnk_$$

cd MCEP_AF_perphone/

treeFile=../phone_trees/STOP$1

#Approximately maximum number of leaves of the tree
num_induced_phones=`wc -l $treeFile.tree| awk '{print $1}'`

cp $treeFile.tree $CG_TMP.tree
for i in `seq 1 $num_induced_phones`
do
    sed '0,/((((\-*[0-9]/s/((((\-*[0-9]/((((ip'${i}') (9/' $CG_TMP.tree > $CG_TMP.treee
    cp $CG_TMP.treee $CG_TMP.tree
done
cp $CG_TMP.tree $treeFile-subbed.tree

rm -f $CG_TMP.*
#rm -f mcep_all af_all